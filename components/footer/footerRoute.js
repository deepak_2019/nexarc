// company routes
export const companyRoutes = [
  {
    link: "https://www.nexarc.in/about-us/",
    routeName: "About us"
  },
  {
    link: "#",
    routeName: "Careers"
  },
  {
    link: "#",
    routeName: "Sitemap"
  },
  {
    link: "#",
    routeName: "Contact us"
  },
]

// legal routes
export const legalRoutes = [
  {
    link: "https://www.nexarc.in/privacy-and-terms/",
    routeName: "Privacy policy"
  },
  {
    link: "https://www.nexarc.in/privacy-and-terms/",
    routeName: "Terms & conditions"
  },
  {
    link: "https://www.nexarc.in/software-used/",
    routeName: "Softwares used"
  },
  {
    link: "https://www.nexarc.in/support/",
    routeName: "Support services"
  },
  {
    link: "https://www.nexarc.in/copy-right-infringement/",
    routeName: "Report copyright infringement"
  },
]

// find opportunities routes
export const findOpportunitiesRoutes = [
  {
    link: "https://www.nexarc.in/tender-and-quotations/",
    routeName: "Tenders & Quotations"
  },
  {
    link: "https://www.nexarc.in/business/",
    routeName: "Business Connections"
  },
  {
    link: "#",
    routeName: "Get Tata Verified"
  },
]

// Get Financing routes
export const getFinancingRoutes = [
  {
    link: "#",
    routeName: "Credit Cards"
  },
  {
    link: "#",
    routeName: "Business Loans"
  },
  {
    link: "#",
    routeName: "Bill Discounting"
  },
]

// Get Solutions routes
export const getSolutionsRoutes = [
  {
    link: "https://www.nexarc.in/product-services/",
    routeName: "Apps & Solutions"
  },
  {
    link: "#",
    routeName: "Managed Services"
  },
]

// Find Experts routes
export const findExpertsRoutes = [
  {
    link: "#",
    routeName: "nexTalks"
  },
  {
    link: "#",
    routeName: "nexConsult"
  },
]

// Get Skilled routes
export const getSkilledRoutes = [
  {
    link: "https://www.nexarc.in/learning/",
    routeName: "nexLearn"
  },
]

// Stay Updated routes
export const stayUpdatedRoutes = [
  {
    link: "https://www.nexarc.in/insights/",
    routeName: "Info Hub"
  },
]