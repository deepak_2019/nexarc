// styles
import "./styles.scss"

import TestimonialSlider from "./testimonialSlider"

function TestimonialSec() {
  return (
    <>
      <div className="testimonial--sec">
        <div className="heading">Hear from our customers</div>
        <TestimonialSlider />
      </div>
    </>
  )
}
export default TestimonialSec;