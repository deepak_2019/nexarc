export const menuItem = [
  {
    menuTitle: "Find Opportunities",
    submenu: [
      {
        title: "Tender & quotations",
        submenuLink: "https://www.nexarc.in/tender-and-quotations/"
      },
      {
        title: "Businesses",
        submenuLink: "https://www.nexarc.in/business/"
      },
      {
        title: "Get Tata Verified",
        submenuLink: "#"
      },
    ]
  },
  {
    menuTitle: "Get Solutions",
    submenu: [
      {
        title: "Apps & Solutions",
        submenuLink: "https://www.nexarc.in/product-services/"
      },
      {
        title: "Manage services",
        submenuLink: "#"
      },
    ]
  },
  {
    menuTitle: "Get Financing",
    submenu: [
      {
        title: "Credit Cards",
        submenuLink: "#"
      },
      {
        title: "Business Loans",
        submenuLink: "#"
      },
      {
        title: "Bill Discounting",
        submenuLink: "#"
      },
    ]
  },
  {
    menuTitle: "Find Experts",
    submenu: [
      {
        title: "nexTalks",
        submenuLink: "#"
      },
      {
        title: "nexConsult",
        submenuLink: "#"
      },
    ]
  },
  {
    menuTitle: "Get Skilled",
    submenu: [
      {
        title: "nexLearn",
        submenuLink: "https://www.nexarc.in/learning/"
      },
    ]
  },
  {
    menuTitle: "Stay Updated",
    submenu: [
      {
        title: "Info Hub",
        submenuLink: "https://www.nexarc.in/insights/"
      },
    ]
  },
]
