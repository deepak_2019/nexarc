// tab 1
export const tab1Content = [
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab1/1.png",
    itemName: "View latest tenders"
  },
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab1/2.png",
    itemName: "Connect with businesses"
  },
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab1/3.png",
    itemName: "Get Tata verified"
  }
]

// tab 2
export const tab2Content = [
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab2/1.png",
    itemName: "Find productivity software"
  },
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab2/2.png",
    itemName: "Get one-on-one growth advice"
  },
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab2/3.png",
    itemName: "Find financial solutions"
  }
]

// tab 3
export const tab3Content = [
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab3/1.png",
    itemName: "Hear from industry experts"
  },
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab3/2.png",
    itemName: "Get business news & tips"
  },
  {
    link: "#",
    itemImg: "../../static/assets/images/services-img/tab3/3.png",
    itemName: "Find courses to enhance skills"
  }
]