export const sliderData = [
  {
    videoLink: "",
    message: "We learnt about GST and e-invoicing in an easy and practical way.",
    userName: "Raghavi Krishna,",
    userDetails: "Platinum Motocorp, IMT Manesar",
  },
  {
    videoLink: "",
    message: "We learnt about GST and e-invoicing in an easy and practical way.",
    userName: "Damayanti Upandhye,",
    userDetails: "Platinum Motocorp, IMT Manesar",
  },
  {
    videoLink: "",
    message: "We learnt about GST and e-invoicing in an easy and practical way.",
    userName: "Karan Kapoor,",
    userDetails: "Manufacturing & production",
  },
  {
    videoLink: "",
    message: "We learnt about GST and e-invoicing in an easy and practical way.",
    userName: "Damayanti Upandhye,",
    userDetails: "Platinum Motocorp, IMT Manesar",
  },
  {
    videoLink: "",
    message: "We learnt about GST and e-invoicing in an easy and practical way.",
    userName: "Karan Kapoor,",
    userDetails: "Manufacturing & production",
  },
  {
    videoLink: "",
    message: "We learnt about GST and e-invoicing in an easy and practical way.",
    userName: "Raghavi Krishna,",
    userDetails: "Platinum Motocorp, IMT Manesar",
  },
]

export const thumbnailData = [
  {
    userImg: "../../static/assets/images/man.png",
    userName: "Raghavi Krishna first",
    userDetails: "Platinum Motocorp, IMT Manesar",
  },
  {
    userImg: "../../static/assets/images/man-2.png",
    userName: "Damayanti Upandhye",
    userDetails: "Platinum Motocorp, IMT Manesar",
  },
  {
    userImg: "../../static/assets/images/man-3.png",
    userName: "Karan Kapoor",
    userDetails: "Manufacturing & production",
  },
  {
    userImg: "../../static/assets/images/man.png",
    userName: "Damayanti Upandhye",
    userDetails: "Platinum Motocorp, IMT Manesar",
  },
  {
    userImg: "../../static/assets/images/man-2.png",
    userName: "Karan Kapoor",
    userDetails: "Manufacturing & production",
  },
  {
    userImg: "../../static/assets/images/man-3.png",
    userName: "Raghavi Krishna",
    userDetails: "Platinum Motocorp, IMT Manesar",
  },
]